$(function () {
    'use strict';

    $(function () {
        $('#dataTableExample').DataTable({
            "aLengthMenu": [
                [10, 30, 50, -1],
                [10, 30, 50, "All"]
            ],
            "iDisplayLength": 10,
            "language": {
                search: "",
                "paginate": {
                    "previous": "Sebelumnya",
                    "next": "Selanjutnya"
                },
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "search": "Cari:",
                "lengthMenu": "Tampilkan _MENU_ entri",
                "zeroRecords": "Tidak ditemukan data yang sesuai",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
            },
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true
        });

        $('#dataTableExample').each(function () {
            var datatable = $(this);
            var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
            search_input.attr('placeholder', 'Cari');
            search_input.removeClass('form-control-sm');
            var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
            length_sel.removeClass('form-control-sm');
        });
    });

    // $(function () {
    //     $('#dataTableTiket').DataTable({
    //         "aLengthMenu": [
    //             [10, 30, 50, -1],
    //             [10, 30, 50, "All"]
    //         ],
    //         "iDisplayLength": 10,
    //         "order": [
    //             [16, "desc"]
    //         ],
    //         "language": {
    //             search: "",
    //             "paginate": {
    //                 "previous": "Sebelumnya",
    //                 "next": "Selanjutnya"
    //             },
    //             "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
    //             "search": "Cari:",
    //             "lengthMenu": "Tampilkan _MENU_ entri",
    //             "zeroRecords": "Tidak ditemukan data yang sesuai",
    //             "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
    //             "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)"
    //         },
    //         "rowReorder": {
    //             selector: 'td:nth-child(2)'
    //         },
    //         "responsive": true,
    //         "columnDefs": [{
    //             "orderable": false,
    //             "targets": 0
    //         }]
    //     });

    //     $('#dataTableTiket').DataTable().on('order.dt search.dt', function () {
    //         $('#dataTableTiket').DataTable().column(0, {
    //             search: 'applied',
    //             order: 'applied'
    //         }).nodes().each(function (cell, i) {
    //             cell.innerHTML = i + 1;
    //         });
    //     }).draw();

    //     $('#dataTableTiket').each(function () {
    //         var datatable = $(this);
    //         var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
    //         search_input.attr('placeholder', 'Search');
    //         search_input.removeClass('form-control-sm');
    //         var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    //         length_sel.removeClass('form-control-sm');
    //     });
    // });

    // $(function () {
    //     $('#dataTableTiketMDK').DataTable({
    //         "aLengthMenu": [
    //             [10, 30, 50, -1],
    //             [10, 30, 50, "All"]
    //         ],
    //         "iDisplayLength": 10,
    //         "order": [
    //             [13, "desc"],
    //             [14, "asc"]
    //         ],
    //         "language": {
    //             search: ""
    //         },
    //         "columnDefs": [{
    //             "orderable": false,
    //             "targets": 0
    //         }]
    //     });

    //     $('#dataTableTiketMDK').DataTable().on('order.dt search.dt', function () {
    //         $('#dataTableTiketMDK').DataTable().column(0, {
    //             search: 'applied',
    //             order: 'applied'
    //         }).nodes().each(function (cell, i) {
    //             cell.innerHTML = i + 1;
    //         });
    //     }).draw();

    //     $('#dataTableTiketMDK').each(function () {
    //         var datatable = $(this);
    //         // SEARCH - Add the placeholder for Search and Turn this into in-line form control
    //         var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
    //         search_input.attr('placeholder', 'Search');
    //         search_input.removeClass('form-control-sm');
    //         // LENGTH - Inline-Form control
    //         var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
    //         length_sel.removeClass('form-control-sm');
    //     });
    // });

});
