<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profil;

class ProfilController extends Controller
{
    public function ubahInformasiPribadi(Request $request)
    {
        $userId = Auth::id();
        $profil = Profil::find($userId);
        $namaLengkap = $request->input('nama_lengkap');
        $noTelepon = $request->input('no_telepon');

        if ($noTelepon == null && $namaLengkap == null) {
            $request->validate([
                'nama_lengkap' => 'required',
                'no_telepon' => 'required',
            ], [
                'nama_lengkap.required' => 'Input Nama Lengkap Belum Diisi!',
                'no_telepon.required' => 'Input No Telepon Belum Diisi!',
            ]);
        } elseif ($noTelepon != null && $namaLengkap == null) {
            $request->validate([
                'no_telepon' => 'string|max:30|regex:/^[0-9]+$/',
            ], [
                'no_telepon.max' => 'Inputan No Telepon Tidak Boleh Lebih Dari 30 Karakter!',
                'no_telepon.regex' => 'Inputan No Telepon Hanya Boleh Diisi Angka!',
            ]);

            $updateNoTelepon = $profil->update([
                'no_telepon' => $noTelepon,
            ]);

            if ($updateNoTelepon) {
                return response()->json([
                    'success' => true,
                    'message' => 'No Telepon Diperbarui!',
                ]);
            }
        } elseif ($noTelepon == null && $namaLengkap != null) {
            $request->validate([
                'nama_lengkap' => 'string|max:20',
            ], [
                'nama_lengkap.max' => 'Inputan Nama Lengkap Tidak Boleh Lebih Dari 20 Karakter!',
            ]);

            $updateNamaLengkap = $profil->update([
                'nama_lengkap' => $namaLengkap,
            ]);

            if ($updateNamaLengkap) {
                return response()->json([
                    'success' => true,
                    'message' => 'Nama Lengkap Diperbarui!',
                ]);
            }
        } elseif ($noTelepon != null && $namaLengkap != null) {
            $request->validate([
                'nama_lengkap' => 'string|max:20',
                'no_telepon' => 'string|max:30|regex:/^[0-9]+$/',
            ], [
                'no_telepon.max' => 'Inputan No Telepon Tidak Boleh Lebih Dari 30 Karakter!',
                'no_telepon.regex' => 'Inputan No Telepon Hanya Boleh Diisi Angka!',
                'nama_lengkap.max' => 'Inputan Nama Lengkap Tidak Boleh Lebih Dari 20 Karakter!',
            ]);

            $infoPribadi = $profil->update([
                'nama_lengkap' => $namaLengkap,
                'no_telepon' => $noTelepon,
            ]);

            if ($infoPribadi) {
                return response()->json([
                    'success' => true,
                    'message' => 'Informasi Pribadi Diperbarui!',
                ]);
            }
        }
    }

    public function ubahInformasiAkun(Request $request)
    {
        $user = Auth::user();
        $email = $request->input('email');

        $request->validate([
            'email' => 'required|email|unique:pengguna,email|max:100',
        ], [
            'email.required' => 'Input Email Belum Diisi!',
            'email.email' => 'Format Email Tidak Valid!',
            'email.unique' => 'Email Yang Diinputkan Sudah Digunakan!',
            'email.max' => 'Inputan Email Tidak Boleh Lebih Dari 100 Karakter!',
        ]);

        $infoAkun = $user->update([
            'email' => $email,
        ]);

        if ($infoAkun) {
            return response()->json([
                'success' => true,
                'message' => 'Informasi Akun Diperbarui!',
            ]);
        }
    }

    public function ubahKataSandi(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'password' => 'required|max:100',
            'konfirmasiPassword' => 'required|same:password',
        ], [
            'password.required' => 'Input Password Belum Diisi!',
            'password.max' => 'Inputan Password Tidak Boleh Lebih Dari 100 Karakter!',
            'konfirmasiPassword.required' => 'Input Konfirmasi Password Belum Diisi!',
            'konfirmasiPassword.same' => 'Password Tidak Sama!',
        ]);

        $password = bcrypt($request->input('password'));

        $user->password = $password;
        $infoPass = $user->save();

        if ($infoPass) {
            return response()->json([
                'success' => true,
                'message' => 'Password Diperbarui!',
            ]);
        }
    }
}
