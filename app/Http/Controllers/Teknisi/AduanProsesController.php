<?php

namespace App\Http\Controllers\Teknisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tiket;
use Illuminate\Support\Facades\Auth;
use App\Models\RiwayatTiket;
use Carbon\Carbon;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class AduanProsesController extends Controller
{
    public function index(Request $request)
    {
        $teknisiId = Auth::id();

        $tikets = Tiket::where('id_pengguna', $teknisiId)
            ->where('status_tiket', 'Sedang Diproses')
            ->get()->map(function ($tiket) {
                foreach ($tiket->getAttributes() as $key => $value) {
                    if ($value === null) {
                        $tiket->{$key} = '---';
                    }
                }

                switch ($tiket->status_tiket) {
                    case 'Selesai':
                        $tiket->status_tiket = '<span class="badge bg-success">Selesai</span>';
                        break;
                    case 'Sedang Diproses':
                        $tiket->status_tiket = '<span class="badge bg-warning">Sedang Diproses</span>';
                        break;
                    case 'Belum Diproses':
                        $tiket->status_tiket = '<span class="badge bg-danger">Belum Diproses</span>';
                        break;
                    default:
                        $tiket->status_tiket = '---';
                }

                $tiket->tanggal_pengerjaan = Carbon::parse($tiket->tanggal_pengerjaan)->format('d/m/Y');
                $tiket->estimasi_selesai = Carbon::parse($tiket->estimasi_selesai)->format('d/m/Y');
                return $tiket;
            });

        foreach ($tikets as $tiket) {
            if ($tiket->nip_dosen !== '---') {
                $tiket->posisi = 'Dosen';
            } elseif ($tiket->nim_mahasiswa !== '---') {
                $tiket->posisi = 'Mahasiswa';
            } elseif ($tiket->nik_karyawan !== '---') {
                $tiket->posisi = 'Karyawan';
            } else {
                $tiket->posisi = 'Tidak Diketahui';
            }
        }

        if ($request->ajax()) {
            $data = Tiket::select([
                'id',
                'nama',
                'no_telepon',
                'deskripsi_tiket',
                'tanggal_pengerjaan',
                'estimasi_selesai',
                'nip_dosen',
                'nim_mahasiswa',
                'nik_karyawan',
                DB::raw("CASE WHEN kategori_laporan = 'Lainnya' THEN kategori_lainnya ELSE kategori_laporan END AS kategori"),
                DB::raw("CASE
                            WHEN nip_dosen IS NOT NULL AND nip_dosen != '' THEN 'Dosen'
                            WHEN nim_mahasiswa IS NOT NULL AND nim_mahasiswa != '' THEN 'Mahasiswa'
                            WHEN nik_karyawan IS NOT NULL AND nik_karyawan != '' THEN 'Karyawan'
                            ELSE 'Tidak Diketahui'
                         END AS posisi")
            ])->where('id_pengguna', $teknisiId)
                ->whereNotNull('tanggal_pengerjaan')
                ->where('status_tiket', 'Sedang Diproses')
                ->with('lampiran')
                ->get();

            $data->each(function ($item) {
                $item->file_tiket = $item->lampiran->first()->file_tiket ?? '---';
            });

            foreach ($data as $tiket) {
                $tiket->tanggal_pengerjaan = Carbon::parse($tiket->tanggal_pengerjaan)->format('d/m/Y');
                $tiket->estimasi_selesai = Carbon::parse($tiket->estimasi_selesai)->format('d/m/Y');
            }

            return Datatables::of($data)->make(true);
        }

        return view(
            '/teknisi/aduan_proses',
            [
                'tikets' => $tikets,
            ]
        );
    }

    public function update(Request $request)
    {
        $tiket = Tiket::where('id', $request->tiketId)->update([
            'status_tiket' => 'Selesai'
        ]);

        $teknisiId = Auth::id();
        $tiketId = $request->input('tiketId');
        $tiket = Tiket::findOrFail($tiketId);

        if ($tiket) {
            RiwayatTiket::create([
                'id_pengguna' => $teknisiId,
                'id_tiket' => $tiket->id,
                'waktu_riwayat' => now(),
                'deskripsi_riwayat' => 'Laporan sudah selesai',
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Tiket telah diselesaikan!',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyelesaikan tiket!',
            ]);
        }
    }
}
