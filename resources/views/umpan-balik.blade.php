<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200" />
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <title>Umpan Balik</title>
</head>

<body>
    @include('partial.navbar')
    <section id="hero-umpanbalik" class="min-md-vh-100">
        <div class="container py-5">
            <h1 class="text-center">Berikan Umpan Balik Anda disini</h1>
            <h1 class="text-center">DO <span class="primary-txt">YOU BEST</span></h1>
            <div class="mx-auto">
                <img src="assets/images/hero-umpan.svg" class="mx-auto d-block w-75" alt="">
            </div>
        </div>
    </section>

    <section id="form-umpanbalik" class="container-fluid p-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div>
                        <img src="assets/images/hubungi.png" alt="">
                    </div>
                    <h1>Sampaikan Umpan Balik Anda</h1>
                    <div class="w-75">
                        <p class="urbanist">Terimakasih telah berkenan membuat umpan balik untuk kami, tindakan Anda
                            sangat membantu bagi
                            kemajuan website kami.</p>
                        <table>
                            <tr>
                                <td><img src="assets/images/maps-icon.png" alt=""></td>
                                <td>
                                    <p class="urbanist ms-2 pt-3">Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang,
                                        Jawa Timur 65147</p>
                                </td>
                            </tr>
                            <tr>
                                <td><img src="assets/images/email-icon.png" alt=""></td>
                                <td>
                                    <p class="urbanist ms-2 pt-3">itskhep@itskdrsprn.ac.id</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <form action="{{ route('tambah.umpan_balik') }}" method="POST">
                        @csrf
                        <div class="row mt-3">
                            <div class="col md-4">
                                <label for="nama" class="form-label urbanist">Nama</label><br>
                                <input type="text" class="form-control urbanist" id="nama" name="nama"
                                    style="width: 300px; height: 50px" placeholder="Nama Kamu">
                            </div>
                            <div class="col md-4">
                                <label for="status" class="form-label urbanist">Status</label><br>
                                <select id="status" name="status" class="form-select urbanist"
                                    style="width: 300px; height: 50px">
                                    <option hidden value="">Mahasiswa/Dosen/Karyawan</option>
                                    <option value="Dosen">Dosen</option>
                                    <option value="Mahasiswa">Mahasiswa</option>
                                    <option value="Karyawan">Karyawan</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col md-4">
                                <label for="email" class="form-label urbanist">Email</label><br>
                                <input type="email" class="form-control urbanist" id="email" name="email"
                                    style="width: 300px; height: 50px" placeholder="Alamat Email">
                            </div>
                            <div class="col md-4">
                                <label for="telepon" class="form-label urbanist">Nomor Telepon</label><br>
                                <input type="number" class="form-control urbanist" id="telepon" name="no_telepon"
                                    style="width: 300px; height: 50px" placeholder="No Telp">
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col md-4">
                                <label for="pesan" class="form-label urbanist">Pesan</label><br>
                                <textarea name="pesan" id="pesan" cols="30" rows="10" class="form-control urbanist"
                                    placeholder="Kritik & Saran"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn w-100 mt-3 py-2 text-white urbanist" id="kirim_umpan_balik"
                            style="background-color: #81D742">Kirim Umpan Balik</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('partial.footer')

</body>

</html>
<script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#kirim_umpan_balik').click(function(e) {
            e.preventDefault();

            var form = $(this).closest('form');
            var formData = form.serialize();

            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: formData,
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil! 👌🥳',
                        text: response.message,
                    });

                    form.trigger('reset');
                },
                error: function(xhr, status, error) {
                    var errors = xhr.responseJSON.errors;
                    var errorMessage = '';
                    $.each(errors, function(key, value) {
                        errorMessage += value[0] + '<br>';
                    });
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal! ✋😌',
                        html: errorMessage,
                    });
                },
            });
        });
    });
</script>
