<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/timeline.css') }}">
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"/>
    <title>Status Tiket</title>
</head>

<body>
<section class="container-fluid">
    @include('partial.navbar')
    <main class="row my-5">
        <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3 shadow rounded-4"
             style="min-height: 70vh">
            <h4 class="fw-bold text-center urbanist mt-5">STATUS TIKET PENGADUAN</h4>
            @if ($count == 0)
                <p class="text-center urbanist mt-5">
                    <span class="text-danger">*</span>
                    Data Riwayat Tidak Ditemukan, Periksa Kembali Kode Tiket
                </p>
            @endif
            <div class="d-flex justify-content-lg-center justify-content-md-start justify-content-center">
                <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 offset-lg-1 mt-5">
                    <ul class="timeline">
                        @foreach ($riwayatTiket as $riwayat)
                            <li class="timeline-item">
                                <div class="timeline-info">
                                    <p>{{ $riwayat->deskripsi_riwayat }}</p>
                                </div>
                                <div class="timeline-marker"></div>
                                <div class="timeline-content">
                                    @if ($loop->iteration == 3)
                                        <p>Estimasi Selesai: {{ $tiket->estimasi_selesai }}</p>
                                        {!! $tiket->note !!} {{-- *masih belum(Green) / telah(Red) --}}
                                    @else
                                        <p>{{ $riwayat->waktu_riwayat }}</p>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </main>
</section>
@include('partial.footer')

<script>
    try {
        f
        Typekit.load({
            async: true
        });
    } catch (e) {
    }
</script>
<script src="https://use.typekit.net/bkt6ydm.js"></script>
<script src="https://kit.fontawesome.com/c3621d3bda.js" crossorigin="anonymous"></script>
</body>

</html>
